#!/bin/bash
#  manageReadFormat.sh  - see below for details
# Copyright (C) 2015-2020,  M. Sangiovanni, ICAR-CNR, Napoli
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.i
#  
# Author: Mara Sangiovanni. Contact:  mara.sangiovanni@icar.cnr.it

# 1) takes in input the file.
# 2) head first 4 lines of the file into a temp file
# 3) search and replace a space followed by a number in the range [0-2] only in the lines beginning
#    with the specified symbol (i.e. @ for fastQ files, > for fasta files). Save the modified file
#    into another temp file
# 4) check if temp1 == temp2. If diff returns 0, they are the same. Assume the old format.
# 5) if diff returns 1 assume new format. Then:
#    5.1) save a backup of the original file.
#    5.2) parse with sed the backup file, and save results into file.
# 6) remove temp files


if (($# == 0)); then
  echo "ERROR No input parameters! CONTACT THE DECONTAMINER TEAM";
fi

fullfile=$1 #input file
subschar=$2 #it will be  either @ (for FASTQ ) or >  (FASTA)
SEP="/"
# write temp files in the file dir
dname=$(dirname "$fullfile")
fname=$(basename "$fullfile")
name="${fname%.*}"

file_excerpt=$dname$SEP$name"_file_excerpt"
file_excerpt_mod=$dname$SEP$name"_file_excerpt_mod"
echo $file_excerpt

# detect if the format is the new one
head -4 $fullfile > $file_excerpt
sed -E "s/(^$subschar.*)\s([0-3]).*/\1\/\2/" $file_excerpt > $file_excerpt_mod
diff -q $file_excerpt $file_excerpt_mod 1>/dev/null
# take the return code
flag_diff=$?
echo flag_diff

if [ $flag_diff = 1 ]; then

  echo " INFO:  processing file $fullfile to manage space in read names (new ILUMINA CASAVA 1.8 format detected)"
  #1)- save a backup copy of the file
  echo " Making a BACKUP copy of the input file..."
  TEMP_PATH=$fullfile
  TEMP_PATH+=".BACKUP"

  cp $fullfile $TEMP_PATH
  echo " ...done. Backup copy saved in $TEMP_PATH"

  # 2) use the backup copy to process the file removing the space
  echo " Now processing the file"
  sed -E "s/(^$subschar.*)\s([0-3]).*/\1\/\2/" $TEMP_PATH > $fullfile

  echo " ...done. File $fullfile modified"
else
  echo "Old Ilumina format detected for read names in  $fullfile headers "
fi
rm $file_excerpt
rm $file_excerpt_mod