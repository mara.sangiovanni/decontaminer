**The decontaMiner pipeline**
Copyright (C) 2015-2020,  M. Sangiovanni and Ilaria Granata,  ICAR-CNR, Napoli 


Authors: dr. Mara Sangiovanni -  mara.sangiovanni@icar.cnr.it
        dr. Ilaria Granata   -  ilaria.granata@icar.cnr.it

Please refer to the pdf UserGuide document for details about installation and usage
the decontaMiner user guide is available online at https://gitlab.com/mara.sangiovanni/decontaminer/

The blast database of contaminating sequences can be downloaded at the following links:


- [BACTERIA DB](https://drive.google.com/drive/folders/1oDpXpm9qN0r-8Y1V9-eZ78afO0JduXXy?usp=sharing)

- [FUNGI DB](https://drive.google.com/drive/folders/1mlH_s30G1R9tWbl3_mulYMMej0afIItM?usp=sharing)

- [VIRUSES](https://drive.google.com/drive/folders/1L7lPkDtkxSjNUHvHdKyH7Wxy0SCzbd0K?usp=sharing)

- [HUMAN RNA](https://drive.google.com/drive/folders/1LRBasjnclIB4WH2h4RwQkT8dxBTrUj2E?usp=sharing)